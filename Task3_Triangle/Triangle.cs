﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Triangle
{
    class Triangle
    {
        #region Fields

        private double m_SideA;
        private double m_SideB;
        private double m_SideC;
        private double m_HalfPeriod;
        private double m_Square;
        private string m_Name;

        #endregion

        #region Properties

        /// <summary>
        /// Name
        /// </summary>
        public string Name
        {
            get { return m_Name; }
        }

        /// <summary>
        /// Side A
        /// </summary>
        public double SideA
        {
            get { return m_SideA; }
        }

        /// <summary>
        /// Side B
        /// </summary>
        public double SideB
        {
            get { return m_SideB; }
        }

        /// <summary>
        /// Side C
        /// </summary>
        public double SideC
        {
            get { return m_SideC; }
        }

        /// <summary>
        /// return the sqare of triangle
        /// </summary>
        public double Square
        {
            get { return m_Square; }
        }

        #endregion

        #region Constructors

        public Triangle(string name, double a, double b, double c)
        {
            this.m_Name = name;
            this.m_SideA = a;
            this.m_SideB = b;
            this.m_SideC = c;

            m_HalfPeriod = (a + b + c) / 2.0;

            m_Square = System.Math.Sqrt(m_HalfPeriod * (m_HalfPeriod - m_SideA) * (m_HalfPeriod - m_SideB) * (m_HalfPeriod - m_SideC));
        }

        #endregion

    }
}
