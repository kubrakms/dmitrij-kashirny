﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            const string END = "End";

            #region Variables

            List<Triangle> listOfTriangle = new List<Triangle>();
            string str = string.Empty;
            string tName = string.Empty;
            double tA = 0;
            double tB = 0;
            double tC = 0;

            #endregion

            while (true)
            {
                #region Enter Data

                Console.Write("\nEnter name of triangle: ");
                tName = Console.ReadLine();
                Console.Write("Enter side A: ");
                str = Console.ReadLine();
                Double.TryParse(str, out tA);
                Console.Write("Enter side B: ");
                str = Console.ReadLine();
                Double.TryParse(str, out tB);
                Console.Write("Enter side C: ");
                str = Console.ReadLine();
                Double.TryParse(str, out tC);

                #endregion

                #region Check for correct data and add to list

                if (tA * tB * tC <= 0)
                {
                    Console.WriteLine("!!! Bad number !!!");
                }
                else
                {
                    listOfTriangle.Add(new Triangle(tName, tA, tB, tC));

                    Console.WriteLine("\n=========== Triangle list ===========");
                    for (int i = 0; i < listOfTriangle.Count; i++)
                    {
                        Console.WriteLine("{0}. [Triangle {1}]: {2:.####} cm", i + 1, listOfTriangle[i].Name, listOfTriangle[i].Square);
                    }
                }

                #endregion

                #region Close application or not

                Console.Write("\n Type \"End\" if you wish close application: ");
                str = Console.ReadLine();
                if (string.Compare(str, END, true) == 0)
                {
                    Environment.Exit(0);
                }

                #endregion

            }
        }
    }
}
