﻿// -----------------------------------------------------------------------
// <copyright file="ICalculatorContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Halp.Modules.Calculator.Abstraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public interface ICalculatorContract : IDisposable
    {
    }
}
