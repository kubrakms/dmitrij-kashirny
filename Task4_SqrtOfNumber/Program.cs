﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_SqrtOfNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            const string LOWER = "lower";
            const string UPPER = "upper";
            int num = 0;
            string str = string.Empty;

            Console.Write("Enter number: ");
            str = Console.ReadLine();
            if (!Int32.TryParse(str, out num))
            {
                Console.WriteLine("!!! Bad number. Try again !!!");
            }
            else
            {
                Console.Write("What whul'd you like see, lower or upper numbers?? (lower/upper): ");
                str = Console.ReadLine();

                if (string.Compare(str, LOWER, true) == 0)
                {
                    for (int i = 1; i * i < num; i++)
                    {
                        Console.Write("{0}, ", i);
                    }
                }
                else if (string.Compare(str, UPPER, true) == 0)
                {
                    for (int i = (int)System.Math.Sqrt(num); i < ((int)System.Math.Sqrt(num) + 5); i++)
                    {
                        Console.Write("{0}, ", i);
                    }
                    Console.Write("....");
                }
                else
                {
                    Console.WriteLine("Wrong choise.");
                }
            }

            Console.ReadLine();
        }
    }
}
