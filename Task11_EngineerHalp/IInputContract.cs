﻿// -----------------------------------------------------------------------
// <copyright file="InputContract.cs" company="Sofserve">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp
{
    using System;
    using System.IO;
    using SoftServe.EngineerHalp.Models;
    using geometry;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IInputContract : IDisposable
    {
        string InputFolder { get; set; }

        string OutputFolder { get; set; }

        bool IsDown { get; }

        event EventHandler OnInputFolderChanged;

        /// <summary>
        /// Provides input Point model functionality
        /// </summary>
        /// <param name="promt">Help string for user</param>
        /// <returns>
        /// <value>Point is entered by user</value>
        /// <value>Default(Point) if input was failed</value>
        /// </returns>
        Gm.Point GetPoint(string promt);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <exception cref="ArgumentException">When Point is ivalid</exception>
        void PrintPoint(Gm.Point point);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="promt"></param>
        /// <returns></returns>
        Command GetCommand(string promt);

        /// <summary>
        /// Saves file to disc
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="path">File path</param>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        /// <exception cref="ArgumentNullException">When path or stream is nallable</exception>
        void SaveFile(Stream stream, string path);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        Stream LoadFile(string path);

        string GetProgramHelp();

        string GetVersion();
    }
}
