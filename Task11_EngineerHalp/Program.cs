﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task11_EngineerHalp
{
    using SoftServe.EngineerHalp;
    using SoftServe.Halp.Modules.Calculator;
    using SoftServe.Halp.Modules.Calculator.Abstraction;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Resources;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;


    class Program
    {
        static void Main(string[] args)
        {
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
            using (var controller = new InputController(Thread.CurrentThread.CurrentUICulture))
            {
                (controller as InputController).Initialize();
                var resManager = new ResourceManager(typeof(global::SoftServe.EngineerHalp.HalpResource));
                Console.WriteLine(controller.GetProgramHelp());

                ICalculatorContract calc = LogicContainer.Instance.GetService<ICalculatorContract>();

                while (!controller.IsDown)
                {
                    var command = controller.GetCommand(resManager.GetString("PromtString", Thread.CurrentThread.CurrentUICulture));

                    if (string.Compare(command.Name, "Compute", true) == 0)
                    {
                        continue;
                    }

                    Console.WriteLine(controller.GetProgramHelp());
                }
                Console.WriteLine(resManager.GetString("FinalMessage", Thread.CurrentThread.CurrentUICulture));
                Console.ReadKey();
            }
        }
    }
}
