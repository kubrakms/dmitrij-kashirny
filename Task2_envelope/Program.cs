﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_envelope
{
    class Program
    {
        static int ParseNum(string str, out float a, out float b, out float c, out float d)
        {
            const string EXIT = "exit";

            string[] param = str.Split(new [] {','});

            for (int i = 0; i < param.Length; i++)
            {
                if (String.Compare(param[i], EXIT, true) == 0)
                {
                    a = 0;
                    b = 0;
                    c = 0;
                    d = 0;
                    return -1;
                }
            }

            if (param.Length != 4)
            {
                a = 0;
                b = 0;
                c = 0;
                d = 0;
                return 1;
            }

            float[] num = new float[param.Length];

            for (int i = 0; i < num.Length; i++)
            {
                if (!float.TryParse(param[i], out num[i]))
                {
                    a = 0;
                    b = 0;
                    c = 0;
                    d = 0;
                    return 1;
                }
            }

            a = num[0];
            b = num[1];
            c = num[2];
            d = num[3];
            
            return 0;
        }

        static void Main(string[] args)
        {
            string strn = string.Empty;
            float a = 0;
            float b = 0;
            float c = 0;
            float d = 0;

            while (true)
            {

                Console.Write("Enter size first envelope (a, b) and second envelope (c, d), for example (2,3,4,5): ");

                strn = Console.ReadLine();

                if (ParseNum(strn, out a, out b, out c, out d) == -1)
                {
                    Environment.Exit(0); ;
                }
                else if (ParseNum(strn, out a, out b, out c, out d) == 1)
                {
                    Console.WriteLine("!!! Bad number !!!");
                }
                else
                {

                    #region Calculate

                    if ((a < c && b < d) || (a == d && b == c))
                    {
                        Console.WriteLine("Envelpe ({0};{1}) can put in envelope ({2};{3})", a, b, c, d);
                    }
                    else
                    {
                        Console.WriteLine("Envelpe ({0};{1}) can not put in envelope ({2};{3})", a, b, c, d);
                    }

                    #endregion
                }

                Console.WriteLine();

            }
        }
    }
}
