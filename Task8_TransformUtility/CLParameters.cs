﻿// -----------------------------------------------------------------------
// <copyright file="CLParameters.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task8_TransformUtility
{
    /// <summary>
    /// Command line parameters
    /// </summary>
    public class CLParameters
    {
        #region Fields

        //Name of input file
        private string m_inputFile;
        //Name of output file
        private string m_OutputFile;
        //Name of log file
        private string m_logFile = "log.txt";
        //Input format
        private string m_inputFormat;
        //Output format
        private string m_outputFormat;
        private List<Triangle> m_Triangles = new List<Triangle>();

        #endregion

        #region Properties

        public List<Triangle> Triangles
        {
            get { return m_Triangles; }
            set { m_Triangles = value; }
        }

        /// <summary>
        /// Get input file name 
        /// </summary>
        public string InputFile
        {
            get { return m_inputFile; }
            set { m_inputFile = value; }
        }

        /// <summary>
        /// Get ouput file name
        /// </summary>
        public string OutputFile
        {
            get { return m_OutputFile; }
            set { m_OutputFile = value; }
        }

        /// <summary>
        /// Get log file name
        /// </summary>
        public string LogFile
        {
            get { return m_logFile; }
            set { m_logFile = value; }
        }

        /// <summary>
        /// Get input file format
        /// </summary>
        public string InputFormat
        {
            get { return m_inputFormat; }
            set { m_inputFormat = value; }
        }

        /// <summary>
        /// Get output file format
        /// </summary>
        public string OutputFormat
        {
            get { return m_outputFormat; }
            set { m_outputFormat = value; }
        }

        #endregion
    }
}
