﻿// -----------------------------------------------------------------------
// <copyright file="LogicContainer.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace Task8_TransformUtility
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Autofac;
    using Autofac.Builder;
    using Task8_TransformUtility;

    /// <summary>
    /// Provides service to creation custom modules with application's logic
    /// </summary>
    public class LogicContainer : IServiceProvider
    {
        private LogicContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<TransformController>().As<ITransformContract>();

            m_Container = builder.Build();
        }

        #region Nested private logic

        private class TypedParameterTypeConverter : ITypeConverter<object, TypedParameter>
        {
            public TypedParameter Convert(ResolutionContext context)
            {
                if (context.SourceValue is ITransformContract)
                    return new TypedParameter(typeof(ITransformContract), context.SourceValue);

                return new TypedParameter(context.SourceType, context.SourceValue);
            }
        }

        #endregion

        #region IServiceProvider implementation

        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(ITransformContract))
                return m_Container.Resolve<ITransformContract>();
            
            return null;
        }

        #endregion

        static LogicContainer()
        {
            ///HINT: aggregate complex the type converter via AutoMapper approach
            Mapper.CreateMap<object, TypedParameter>().ConvertUsing(new TypedParameterTypeConverter());
        }

        #region Thread safe singletone

        public static LogicContainer Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (m_Instance == null)
                        m_Instance = new LogicContainer();
                }
                return m_Instance;
            }
        }

        #endregion

        public T GetService<T>()
        {
            return m_Container.Resolve<T>();
        }

        public T GetService<T>(object[] parameters)
        {
            if (parameters == null)
                return GetService<T>();

            var @params = Mapper.Map<object[], IEnumerable<TypedParameter>>(parameters);
            return m_Container.Resolve<T>(@params);
        }

        private static readonly object __mutex = new object();
        private static LogicContainer m_Instance = default(LogicContainer);

        private readonly IContainer m_Container = default(IContainer);
    }
}
