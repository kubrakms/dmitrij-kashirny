﻿// -----------------------------------------------------------------------
// <copyright file="CommandLineArgumentParser.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    public class CommandLineArgumentParser
    {
        #region Methods

        /// <summary>
        /// Parse command line argumets 
        /// </summary>
        /// <param name="args">Array arguments</param>
        public static CLParameters ParseArgs(string[] args)
        {
            CLParameters parameters = new CLParameters();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Contains(resource.IN))
                {
                    parameters.InputFile = args[i].Replace(resource.IN, string.Empty);
                }
                else if (args[i].Contains(resource.OUT))
                {
                    parameters.OutputFile = args[i].Replace(resource.OUT, string.Empty);
                }
                else if (args[i].Contains(resource.FROM))
                {
                    parameters.InputFormat = args[i].Replace(resource.FROM, string.Empty);
                }
                else if (args[i].Contains(resource.TO))
                {
                    parameters.OutputFormat = args[i].Replace(resource.TO, string.Empty);
                }
                else if (args[i].Contains(resource.LOG))
                {
                    parameters.LogFile = args[i].Replace(resource.LOG, string.Empty);
                }
            }
            return parameters;
        }

        #endregion
    }
}
