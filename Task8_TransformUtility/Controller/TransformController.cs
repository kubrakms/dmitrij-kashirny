﻿// -----------------------------------------------------------------------
// <copyright file="TransformController.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    using System.Collections.Generic;

    /// <summary>
    /// Transform controller
    /// </summary>
    internal class TransformController : ITransformContract
    {
        #region ITransformContract Members

        public List<Triangle> Read(string inputFile, string inputFormat)
        {
            List<Triangle> temp = new List<Triangle>();

            using (var reader = FormatFactory<Triangle>.GetInstance(inputFormat))
            {
                temp = reader.Read(inputFile);
            }

            return temp;
        }

        public void Write(string outputFile, string outputFormat, List<Triangle> data)
        {
            using (var writer = FormatFactory<Triangle>.GetInstance(outputFormat))
            {
                writer.Write(outputFile, data);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
