﻿// -----------------------------------------------------------------------
// <copyright file="CSVReadWrite.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// CSV Serialezer/Desirealizer class
    /// </summary>
    public class CSVReadWrite<T> : IReadWrite<T>
    {
        #region IReadWrite<T> Members

        /// <summary>
        /// Desirealize List of types T from .csv file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <returns>List of types T</returns>
        public List<T> Read(string fileName)
        {
            List<T> result = new List<T>();
            string str = string.Empty;

            using (StreamReader rd = new StreamReader(fileName))
            {
                while ((str = rd.ReadLine()) != null)
                {
                    Type typeT = typeof(T);
                    try
                    {
                        MethodInfo parseMethodInfo = typeT.GetMethod("ParseTriangle");
                        T element = (T)(parseMethodInfo.Invoke(null, new object[] { str }));
                        result.Add(element);
                    }
                    catch
                    {
                        return null;
                    } 
                }
            }
            return result;
        }

        /// <summary>
        /// Serialize List of types T in .csv file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <param name="data">List of types T</param>
        public void Write(string fileName, List<T> data)
        {
            using (StreamWriter wr = new StreamWriter(fileName))
            {
                foreach (T item in data)
                {
                    wr.WriteLine(item.ToString());
                }
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
