﻿// -----------------------------------------------------------------------
// <copyright file="BINReadWriter.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Task8_TransformUtility
{
    /// <summary>
    /// Binary Serialezer/Desirealizer class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BINReadWriter<T> : IReadWrite<T>
    {
        private BinaryFormatter myFormater = new BinaryFormatter();

        #region IReadWrite<T> Members

        /// <summary>
        /// Desirealize List of types T from binary file.
        /// </summary>
        /// <param name="fileName">file with will be read</param>
        /// <returns>List of types T</returns>
        public List<T> Read(string fileName)
        {
            List<T> result = new List<T>();

            using (Stream reader = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                result = (List<T>)myFormater.Deserialize(reader);
            }

            return result;
        }

        /// <summary>
        /// Serialize List of types T in binary file
        /// </summary>
        /// <param name="fileName">input file</param>
        /// <param name="data">List of types T</param>
        public void Write(string fileName, List<T> data)
        {
            using (Stream writer = new FileStream(fileName, FileMode.Create))
            {
                myFormater.Serialize(writer, data);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
