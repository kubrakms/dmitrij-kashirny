﻿// -----------------------------------------------------------------------
// <copyright file="XMLReadWriter.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace Task8_TransformUtility
{
    /// <summary>
    /// XML Serializer/Desirealizer class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class XMLReadWriter<T> : XmlSerializer, IReadWrite<T>
    {
        private XmlSerializer xwr = new XmlSerializer(typeof(List<T>));

        #region IReadWrite<T> Members

        /// <summary>
        /// Desirealize List of types T from .xml file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <returns>List of types T</returns>
        List<T> IReadWrite<T>.Read(string fileName)
        {
            List<T> result = new List<T>();

            using (FileStream rd = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                result = (List<T>)xwr.Deserialize(rd);
            }
            return result;
        }

        /// <summary>
        /// Serialize List of types T in .xml file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <param name="data">List of types T</param>
        public void Write(string fileName, List<T> data)
        {
            using (StreamWriter wr = new StreamWriter(fileName))
            {
                xwr.Serialize(wr, data);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
