﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;

namespace Task8_TransformUtility
{
    class Program
    {
        const short SUCSSESS_EXIT_CODE = 0;
        const short FAULT_EXIT_CODE = -21312;
        const short NAME_MAX_LENGHT = 40;
        static CLParameters parameters = new CLParameters();

        static void Main(string[] args)
        {
            string str = string.Empty;

            if (args.Length == 0)
            {
                Console.WriteLine(resource.HelpResource);
                Console.Write(resource.EnterOutputFormat);
                parameters.OutputFormat = Console.ReadLine();

                while (true)
                {
                    if (string.Compare(str, resource.EXIT, true) == 0)
                    {
                        var wr = FormatFactory<Triangle>.GetInstance(parameters.OutputFormat);
                        wr.Write(string.Format("{0}.{1}", resource.DefaultOutputFileName, parameters.OutputFormat), parameters.Triangles);
                        Environment.Exit(SUCSSESS_EXIT_CODE);
                    }
                    parameters.Triangles.Add(GetTriangle());
                    Console.Write(resource.ContinuePromt);
                    str = Console.ReadLine();
                }
            }
            else
            {
                CLParameters parameters = CommandLineArgumentParser.ParseArgs(args);

                if (parameters.InputFile == null | parameters.InputFormat == null |
                    parameters.OutputFile == null | parameters.OutputFormat == null |
                    !File.Exists(parameters.InputFile))
                {
                    Loger.Instance.AppendToLog(parameters.LogFile, resource.InvalidArguments);
                    Environment.Exit(FAULT_EXIT_CODE);
                }

                using (var transformer = LogicContainer.Instance.GetService<ITransformContract>())
                {
                    List<Triangle> temp = new List<Triangle>();
                    temp = transformer.Read(parameters.InputFile, parameters.InputFormat);
                    transformer.Write(parameters.OutputFile, parameters.OutputFormat, temp);
                }
            }
            Environment.Exit(SUCSSESS_EXIT_CODE);
        }

        /// <summary>
        /// Get instanse of Triangle class from Console
        /// </summary>
        /// <returns></returns>
        static Triangle GetTriangle()
        {
            string str = string.Empty;
            Triangle tempTriangle = new Triangle();

            Console.Write(resource.TriangleId);
            str = Console.ReadLine();
            tempTriangle.Id = Int32.Parse(str);

            Console.Write(resource.TriangleName);
            str = Console.ReadLine();
            if (str.Length > NAME_MAX_LENGHT)
            {
                Loger.Instance.AppendToLog(parameters.LogFile, resource.NameToLarge);
                str = str.Remove(NAME_MAX_LENGHT);
            }
            tempTriangle.Name = str;

            Console.Write(resource.TrianglePoint1);
            str = Console.ReadLine();
            tempTriangle.V1 = tempTriangle.Parse(str);

            Console.Write(resource.TrianglePoint2);
            str = Console.ReadLine();
            tempTriangle.V2 = tempTriangle.Parse(str);

            Console.Write(resource.TrianglePoint3);
            str = Console.ReadLine();
            tempTriangle.V3 = tempTriangle.Parse(str);

            return tempTriangle;
        }
    }
}




//[TestCase("", 1.0, Result = 1)]
//                 yield return new TestCaseData(list, filename);
//             var provider = new MD5CryptoServiceProvider();
//            using (var ethalon = new FileStream(ethalonFile, FileMode.Open))
//            {
//                using (var test = new FileStream(testFile, FileMode.Open))
//                {
//                    Assert.AreEqual(provider.ComputeHash(ethalon), provider.ComputeHash(test));

//                }
//            }
//        public IEnumerable<TestCaseData> getPackage
//        {
//            get
//            {
//                int quantity = 5;
//                var list = new List<HalpFormatData>();
//                for (int i = 0; i < quantity; i++)
//                {
//                    list.Add(new HalpFormatData()
//                        {
//                            ID = i,
//                            Vertex1 = new geometry.Gm.Point(1, 1, 1),
//                            Vertex2 = new geometry.Gm.Point(2,2,2),
//                            Vertex3 = new geometry.Gm.Point(3,3,3)
//                        });
//                }
//                yield return new TestCaseData(list);
//                yield return new TestCaseData(list);
//                yield return new TestCaseData(list);
//            }
//        }