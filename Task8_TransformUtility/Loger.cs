﻿// -----------------------------------------------------------------------
// <copyright file="Loger.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    using System;
    using System.IO;

    /// <summary>
    /// Class for loging
    /// </summary>
    public class Loger
    {
        private Loger() { }

        /// <summary>
        /// Add string to log file
        /// </summary>
        /// <param name="fileName">Name of log file</param>
        /// <param name="message">String of message</param>
        public void AppendToLog( string fileName, string message)
        {
            using (StreamWriter writer = new StreamWriter(fileName, true))
            {
                writer.WriteLine("{0}: {1}", DateTime.Now, message);
            }
        }

        public static Loger Instance
        {
            get
            {
                if (Loger.m_instance == null)
                {
                    lock (_mutex)
                    {
                        if (Loger.m_instance == null)
                        {
                            Loger.m_instance = new Loger();
                        }
                    }
                }
                return Loger.m_instance;
            }
        }

        private static Loger m_instance;
        private static object _mutex = new object();

    }
}
