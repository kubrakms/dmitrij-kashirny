﻿// -----------------------------------------------------------------------
// <copyright file="Triangle.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    using System;
    using geometry;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class of triangle
    /// </summary>
    [Serializable()]
    [DataContract]
    public class Triangle
    {
        #region Constructors

        /// <summary>
        /// Default constryctor
        /// </summary>
        public Triangle() { }

        /// <summary>
        /// Parameterles constructor
        /// </summary>
        /// <param name="id">Id of triangle</param>
        /// <param name="name">Name of triangle</param>
        /// <param name="p1">Coordinates of first point</param>
        /// <param name="p2">Coordinates of second point</param>
        /// <param name="p3">Coordinates of third point</param>
        public Triangle(int id, string name, Gm.Point p1, Gm.Point p2, Gm.Point p3)
        {
            Id = id;
            Name = name;
            V1 = p1;
            V2 = p2;
            V3 = p3;
        }

        #endregion
        
        /// <summary>
        /// Parse coordinates from string
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>instanse of Gm.Point class</returns>
        public Gm.Point Parse(string str)
        {
            double x = 0;
            double y = 0;
            double z = 0;
            string[] array = new string[3];
            
            array = str.Split(new char[] { ';', ',', ' '});

            Double.TryParse(array[0], out x);
            Double.TryParse(array[1], out y);
            Double.TryParse(array[2], out z);

            return new Gm.Point(x, y, z);
        }

        /// <summary>
        /// Parse triangle from string
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>Instanse of Triangle class</returns>
        public static Triangle ParseTriangle(string str)
        {
            Triangle tempTriangle = new Triangle();

            string[] array = str.Split(new char[] { ';' });

            tempTriangle.Id = Int32.Parse(array[0]);
            tempTriangle.Name = array[1];
            tempTriangle.V1 = tempTriangle.Parse(string.Format("{0} {1} {2}", array[2], array[3], array[4]));
            tempTriangle.V2 = tempTriangle.Parse(string.Format("{0} {1} {2}", array[5], array[6], array[7]));
            tempTriangle.V3 = tempTriangle.Parse(string.Format("{0} {1} {2}", array[8], array[9], array[10]));

            return tempTriangle;
        }

        /// <summary>
        /// Overriden method ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0};{1};{2:F4};{3:F4};{4:F4};{5:F4};{6:F4};{7:F4};{8:F4};{9:F4};{10:F4}",
                Id, Name, V1.x, V1.y, V1.z, V2.x, V2.y, V2.z, V3.x, V3.y, V3.z);
        }

        #region Properties

        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Gm.Point V1 { get; set; }
        
        [DataMember]
        public Gm.Point V2 { get; set; }
        
        [DataMember]
        public Gm.Point V3 { get; set; }

        #endregion
    }
}
