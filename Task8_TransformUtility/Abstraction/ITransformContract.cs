﻿// -----------------------------------------------------------------------
// <copyright file="ITransformContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Transform contract
    /// </summary>
    public interface ITransformContract : IDisposable
    {
        /// <summary>
        /// Read list of Triangles from file
        /// </summary>
        /// <param name="inputFile">input file name</param>
        /// <param name="inputFormat">input file format</param>
        /// <returns>List if Triangles</returns>
        List<Triangle> Read(string inputFile, string inputFormat);
        
        /// <summary>
        /// Write List of Triangles to file
        /// </summary>
        /// <param name="outputFile">Output file name</param>
        /// <param name="outputFormat">Output fike format</param>
        /// <param name="data">List of Triangles</param>
        void Write (string outputFile, string outputFormat, List<Triangle> data);
    }
}
