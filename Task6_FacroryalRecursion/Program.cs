﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6_FacroryalRecursion
{
    class Program
    {
        static public ulong Factorial(uint n)
        {
            if (n == 0)
            {
                return 1;
            }
            return n * Factorial(n - 1);
        }

        static void Main(string[] args)
        {
            string str = string.Empty;
            uint num = 0;

            while (true)
            {

                Console.Write("Enter number: ");
                str = Console.ReadLine();

                if (!UInt32.TryParse(str, out num))
                {
                    Console.WriteLine("!!! Bad number !!!");
                }
                else
                {
                    Console.WriteLine("{0}! = {1}", num, Factorial(num));
                }

            }

            Console.ReadLine();
        }
    }
}
