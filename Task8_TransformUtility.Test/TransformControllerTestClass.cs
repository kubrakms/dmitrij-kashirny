﻿// -----------------------------------------------------------------------
// <copyright file="TransformTestClass.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility.Test
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.IO;
    using geometry;
    using System.Security.Cryptography;

    /// <summary>
    /// Tests for Transform controller methods
    /// </summary>
    [TestFixture]
    public class TransformControllerTestClass
    {
        [SetUp]
        public void Initalize()
        {
            var assemblyPath = typeof(TransformControllerTestClass).Assembly.Location;
            RootDataPath = Path.Combine(Path.GetDirectoryName(assemblyPath), "Data");
        }

        [Test(Description = "Examines ReadFormat functionality"), TestCaseSource("ReadFormatActionData")]
        public void ReadTest(string inputFile, string inputFormat, Triangle etalon)
        {
            var filePath = string.Format("{0}\\{2}\\{1}",
                new object[] { RootDataPath, inputFile, "ReadData" });

            Assert.IsTrue(File.Exists(filePath));

            using (var transformer = LogicContainer.Instance.GetService<ITransformContract>())
            {
                var reader = FormatFactory<Triangle>.GetInstance(inputFormat);
                var actual = reader.Read(filePath)[0];
                var cmp = new TriangleComparer();
                Assert.IsTrue(cmp.Compare(etalon, actual) == 0);
            }
        }

        [Test(Description = "Examines WriteFormat functionality"), TestCaseSource("WriteFormatActionData")]
        public void WriteTest(string outputFile, string outputFormat, string etalonFileName, Triangle toWrite)
        {
            var filePath = string.Format("{0}\\{2}\\{1}",
                new object[] { RootDataPath, outputFile, "WriteData" });

            var etalonfilePath = string.Format("{0}\\{2}\\{1}",
                new object[] { RootDataPath, etalonFileName, "WriteData" });

            Assert.IsTrue(File.Exists(etalonfilePath));

            using (var writer = LogicContainer.Instance.GetService<ITransformContract>())
            {
                List<Triangle> temp = new List<Triangle>();
                temp.Add(toWrite);
                writer.Write(filePath, outputFormat, temp);
            }

            var provider = new MD5CryptoServiceProvider();
            using (var ethalon = new FileStream(etalonfilePath, FileMode.Open))
            {
                using (var test = new FileStream(filePath, FileMode.Open))
                {
                    Assert.AreEqual(provider.ComputeHash(ethalon), provider.ComputeHash(test));
                }
            }
        }

        public static IEnumerable<TestCaseData> ReadFormatActionData
        {
            get
            {
                yield return new TestCaseData("ReadData.xml", "xml",
                    new Triangle()
                        {
                            Id = 1,
                            Name = "Template1",
                            V1 = new Gm.Point() { x = 12, y = 13, z = 0 },
                            V2 = new Gm.Point() { x = 13, y = 12, z = 0 },
                            V3 = new Gm.Point() { x = 0, y = 0, z = 0 }
                        });


                yield return new TestCaseData("ReadData.json", "json",
                   new Triangle()
                   {
                       Id = 1,
                       Name = "Template1",
                       V1 = new geometry.Gm.Point() { x = 12, y = 13, z = 0 },
                       V2 = new geometry.Gm.Point() { x = 13, y = 12, z = 0 },
                       V3 = new geometry.Gm.Point() { x = 0, y = 0, z = 0 },
                   });

                yield return new TestCaseData("ReadData.csv", "csv",
                    new Triangle()
                    {
                        Id = 1,
                        Name = "Template1",
                        V1 = new Gm.Point() { x = 12, y = 13, z = 0 },
                        V2 = new Gm.Point() { x = 13, y = 12, z = 0 },
                        V3 = new Gm.Point() { x = 0, y = 0, z = 0 }
                    });


                yield return new TestCaseData("ReadData.dat", "dat",
                   new Triangle()
                   {
                       Id = 1,
                       Name = "Template1",
                       V1 = new geometry.Gm.Point() { x = 12, y = 13, z = 0 },
                       V2 = new geometry.Gm.Point() { x = 13, y = 12, z = 0 },
                       V3 = new geometry.Gm.Point() { x = 0, y = 0, z = 0 },
                   });
            }
        }

        public static IEnumerable<TestCaseData> WriteFormatActionData
        {
            get
            {
                yield return new TestCaseData("WriteData.xml", "xml", "etalon.xml",
                     new Triangle()
                     {
                         Id = 1,
                         Name = "Template1",
                         V1 = new geometry.Gm.Point() { x = 11, y = 21, z = 31 },
                         V2 = new geometry.Gm.Point() { x = 12, y = 22, z = 32 },
                         V3 = new geometry.Gm.Point() { x = 13, y = 23, z = 33 },
                     });

                yield return new TestCaseData("WriteData.json", "json", "etalon.json",
                     new Triangle()
                     {
                         Id = 1,
                         Name = "Template1",
                         V1 = new geometry.Gm.Point() { x = 11, y = 21, z = 31 },
                         V2 = new geometry.Gm.Point() { x = 12, y = 22, z = 32 },
                         V3 = new geometry.Gm.Point() { x = 13, y = 23, z = 33 },
                     });

                yield return new TestCaseData("WriteData.csv", "csv", "etalon.csv",
                     new Triangle()
                     {
                         Id = 1,
                         Name = "Template1",
                         V1 = new geometry.Gm.Point() { x = 11, y = 21, z = 31 },
                         V2 = new geometry.Gm.Point() { x = 12, y = 22, z = 32 },
                         V3 = new geometry.Gm.Point() { x = 13, y = 23, z = 33 },
                     });

                yield return new TestCaseData("WriteData.dat", "dat", "etalon.dat",
                     new Triangle()
                     {
                         Id = 1,
                         Name = "Template1",
                         V1 = new geometry.Gm.Point() { x = 11, y = 21, z = 31 },
                         V2 = new geometry.Gm.Point() { x = 12, y = 22, z = 32 },
                         V3 = new geometry.Gm.Point() { x = 13, y = 23, z = 33 },
                     });
            }
        }
        
        private string RootDataPath = string.Empty;
    }
}
