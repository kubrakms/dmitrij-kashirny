﻿// -----------------------------------------------------------------------
// <copyright file="TriangleComparer.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task8_TransformUtility.Test
{
    using System.Collections.Generic;

    /// <summary>
    /// Class for compare triangles
    /// </summary>
    internal class TriangleComparer : IComparer<Triangle>
    {
        /// <summary>
        /// Compare triangles
        /// </summary>
        /// <param name="element1">First triangle</param>
        /// <param name="element2">Second triangle</param>
        /// <returns></returns>
        public int Compare(Triangle element1, Triangle element2)
        {
            if (element1 == null | element2 == null)
                return -1;

            if (element1.Id != element2.Id)
                return -1;

            if (string.Compare(element1.Name, element2.Name) != 0)
                return -1;

            if (!element2.V1.Equals(element2.V1) | element2.V2.Equals(element2.V2) | element2.V3.Equals(element2.V3))
                return -1;

            return 0;
        }
    }
}
