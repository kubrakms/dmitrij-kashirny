﻿// -----------------------------------------------------------------------
// <copyright file="eFormatType.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp.Proxies
{
    public enum eFormatType: byte
    {
        JSON = 0,
        XML = 1,
        Binary = 2,
        CSV = 3
    }
}
