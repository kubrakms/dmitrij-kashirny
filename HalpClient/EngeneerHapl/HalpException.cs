﻿// -----------------------------------------------------------------------
// <copyright file="HalpException.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp
{
    using System;

    [Serializable]
    public class HalpException : Exception
    {
        public HalpException() { }
        public HalpException(string message) : base(message) { }
        public HalpException(string message, Exception inner) : base(message, inner) { }
        protected HalpException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
