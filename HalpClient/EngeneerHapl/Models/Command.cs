﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp.Models
{

    /// <summary>
    /// Command class
    /// </summary>
    public class Command
    {
        public string Name { get; set; }

        public object[] Params { get; set; }
    }
}
