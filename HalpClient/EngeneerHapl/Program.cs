﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp
{
    using System;
    using System.Resources;
    using System.Threading;
    using System.Globalization;
    using SoftServe.HalpWeb.Client.EngeneerHalp.Controller;

    class Program
    {
        static void Main(string[] args)
        {
            string str = string.Empty;
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-Ru");
            CultureInfo currentUi = Thread.CurrentThread.CurrentUICulture;

            using (var controller = new InputController(currentUi))
            {
                (controller as InputController).Initialize();
                controller.CheckFolders();

                var resManager = new ResourceManager(typeof(global::SoftServe.HalpWeb.Client.EngeneerHalp.HalpResource));
                Console.WriteLine(controller.GetProgramHelp());

                while (!controller.IsDown)
                {
                    var command = controller.GetCommand(resManager.GetString("PromtString", currentUi));

                    if (string.Compare(command.Name.ToLower(), resManager.GetString("Help", currentUi)) == 0)
                    {
                        Console.WriteLine(controller.GetProgramHelp());
                    }
                    else if (string.Compare(command.Name.ToLower(), resManager.GetString("Ver", currentUi)) == 0)
                    {
                        Console.WriteLine(controller.GetVersion());
                    }
                    else if (string.Compare(command.Name.ToLower(), resManager.GetString("Create", currentUi)) == 0)
                    {
                        Console.Write(resManager.GetString("FileNamePromt", currentUi));
                        string inFile = Console.ReadLine();
                        controller.SaveFile(inFile);
                    }
                    else if (string.Compare(command.Name.ToLower(), resManager.GetString("Transform", currentUi)) == 0)
                    {
                        
                    }
                    else if (string.Compare(command.Name.ToLower(), resManager.GetString("Compute", currentUi)) == 0)
                    {
                                           }
                    else
                    {
                        Console.WriteLine(controller.GetProgramHelp());
                    }
                }
                Console.WriteLine(resManager.GetString("FinalMessage", currentUi));
                Console.ReadKey();
            }
        }
    }
}
