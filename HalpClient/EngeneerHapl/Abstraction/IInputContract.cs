﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp.Abstraction
{
    using System;
    using geometry;
    using System.IO;
    using SoftServe.HalpWeb.Client.EngeneerHalp.Models;

    /// <summary>
    /// Input Contract
    /// </summary>
    public interface IInputContract : IDisposable
    {
        string InputFolder { get; }
        
        string OutputFolder { get; }

        bool IsDown { get; }

        event EventHandler OnInputFolderChanged;

        /// <summary>
        /// Provides input Point model functionality
        /// </summary>
        /// <param name="promt">Help string for user</param>
        /// <returns>
        /// <value>Point is entered by user</value>
        /// <value>Default(Point) if input was failed</value>
        /// </returns>
        Gm.Point GetPoint(string promt);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <exception cref="ArgumentException">When Point is ivalid</exception>
        void PrintPoint(Gm.Point point);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="promt"></param>
        /// <returns></returns>
        Command GetCommand(string promt);

        /// <summary>
        /// Saves file to disc
        /// </summary>
        /// <param name="path">File path</param>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        /// <exception cref="ArgumentNullException">When path or stream is nallable</exception>
        void SaveFile(string path);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        Stream LoadFile(string path);

        /// <summary>
        /// Get help
        /// </summary>
        /// <returns>String with help message</returns>
        string GetProgramHelp();

        /// <summary>
        /// Get version
        /// </summary>
        /// <returns>String with version message</returns>
        string GetVersion();

        /// <summary>
        /// Get string message about status executing command
        /// </summary>
        /// <param name="code">Code</param>
        /// <returns>String message</returns>
        string CommandExecuteStatus(int code);
    }
}
