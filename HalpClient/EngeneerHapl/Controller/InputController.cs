﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Client.EngeneerHalp.Controller
{
    using System;
    using System.IO;
    using System.Globalization;
    using System.Resources;
    using SoftServe.HalpWeb.Client.EngeneerHalp.Abstraction;
    using SoftServe.HalpWeb.Client.EngeneerHalp.Models;

    /// <summary>
    /// Input controller
    /// </summary>
    internal class InputController : IInputContract
    {
        public InputController(CultureInfo culture)
        {
            if (culture != default(CultureInfo))
                mCulture = culture;
        }

        public void Initialize()
        {
            mResManager = new ResourceManager(typeof(global::SoftServe.HalpWeb.Client.EngeneerHalp.HalpResource));
            m_InputFolder = mResManager.GetString("InputFolder");
            m_OutputFolder = mResManager.GetString("OutputFolder");
        }

        public void CheckFolders()
        {
            if (!Directory.Exists(m_InputFolder))
            {
                Directory.CreateDirectory(m_InputFolder);
            }

            if (!Directory.Exists(m_OutputFolder))
            {
                Directory.CreateDirectory(m_OutputFolder);
            }
        }

        public string InputFolder
        {
            get { return m_InputFolder; }
        }

        public string OutputFolder
        {
            get { return m_OutputFolder; }
        }

        public bool IsDown
        {
            get;
            private set;
        }

        public event EventHandler OnInputFolderChanged;

        public geometry.Gm.Point GetPoint(string promt)
        {
            throw new NotImplementedException();
        }

        public Command GetCommand(string promt)
        {
            Console.Write(promt);
            var line = Console.ReadLine();

            var command = new Command() { Name = line };
            IsDown = (string.Compare(command.Name, mResManager.GetString("ExitName", mCulture) , true) == 0);

            return command;
        }

        public void PrintPoint(geometry.Gm.Point point)
        {
            throw new NotImplementedException();
        }

        public void SaveFile(string path)
        {
            path = string.Format("{0}\\{1}", m_InputFolder, path);
            using (FileStream writer = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                
            }
        }

        public System.IO.Stream LoadFile(string path)
        {
            throw new NotImplementedException();
        }

        public string GetProgramHelp()
        {
            return mResManager.GetString("HelpString", mCulture);
        }

        public string GetVersion()
        {
            return mResManager.GetString("VersionString", mCulture);
        }

        public string CommandExecuteStatus(int code)
        {
            switch (code)
            {
                case SUCSSES:
                    return mResManager.GetString("Done", mCulture);
                case ERROR_TRANSFORM:
                    return mResManager.GetString("ErrorTransform", mCulture);
                case ERROR_MODULE:
                    return mResManager.GetString("ModuleNotFound", mCulture);
                default:
                    return mResManager.GetString("UnknownError", mCulture);
            }
        }

        public void Dispose()
        {
            // throw new NotImplementedException();
        }

        private const short ERROR_TRANSFORM = -21312;
        private const short SUCSSES = 0;
        private const short ERROR_MODULE = -1;
        private readonly CultureInfo mCulture = new CultureInfo("en-US");
        private ResourceManager mResManager = default(ResourceManager);
        private string m_InputFolder = string.Empty;
        private string m_OutputFolder = string.Empty;
    }
}
