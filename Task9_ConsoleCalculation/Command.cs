﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System.Resources;

    /// <summary>
    /// Command class
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Name of command
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Parameters
        /// </summary>
        public string[] Params { get; set; }

        /// <summary>
        /// Input file name
        /// </summary>
        public static string inputFile 
        { 
            get { return mInputFile; }
            set { mInputFile = value; }
        }

        /// <summary>
        /// Output file name
        /// </summary>
        public static string outputFile 
        { 
            get { return mOutputFile; }
            set { mOutputFile = value; }
        }

        /// <summary>
        /// Transform matrix
        /// </summary>
        public static double[] Matrix { get { return m_matrix; } set { m_matrix = value; } }

        /// <summary>
        /// Command Resources
        /// </summary>
        public static ResourceManager CommandStrings { get { return resManager; } }

        /// <summary>
        /// Parse command line arguments. Allocation in and out files names
        /// </summary>
        /// <param name="args">Array of strings</param>
        /// <returns>true if parse ok</returns>
        public bool ParseArgs(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Contains(resManager.GetString("IN_FILE")))
                {
                    mInputFile = args[i].Replace(resManager.GetString("IN_FILE"), string.Empty);
                    continue;
                }
                else if (args[i].Contains(resManager.GetString("OUTPUT_FILE")))
                {
                    mOutputFile = args[i].Replace(resManager.GetString("OUTPUT_FILE"), string.Empty);
                    continue;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private static readonly ResourceManager resManager = new ResourceManager(typeof(Resourses));

        private static string mInputFile;
        private static string mOutputFile;
        private static double[] m_matrix = { 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0 };
    }
}
