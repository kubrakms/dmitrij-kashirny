﻿// -----------------------------------------------------------------------
// <copyright file="Result.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System.Collections.Generic;
    using geometry;

    /// <summary>
    /// Result class
    /// </summary>
    public class Result
    {
        public Result()
        {
            Points = new List<Gm.Point>();
        }

        public List<Gm.Point> Points { get; set; }
        public int Error { get; set; }
    }
}
