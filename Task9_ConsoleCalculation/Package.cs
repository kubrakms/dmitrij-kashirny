﻿// -----------------------------------------------------------------------
// <copyright file="Package.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{

    /// <summary>
    /// Package mode
    /// </summary>
    public class Package : IMode
    {
        private string[] m_Args;
        public string[] Args
        {
            get { return m_Args; }
            set { m_Args = value; }
        }

        public Package(string[] args)
        {
            Args = args;
        }
    }
}
