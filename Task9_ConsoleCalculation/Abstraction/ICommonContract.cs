﻿// -----------------------------------------------------------------------
// <copyright file="ICommonContract.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{

    /// <summary>
    /// Common interface
    /// </summary>
    public interface ICommonContract : IInputContract, IOutputContract, ITransformContract
    {
    }
}
