﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using geometry;

    /// <summary>
    /// Input contract
    /// </summary>
    public interface IInputContract : IDisposable
    {
        /// <summary>
        /// Get the point
        /// </summary>
        /// <returns>Result</returns>
        Result GetPoint();
    }
}
