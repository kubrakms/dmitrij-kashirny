﻿// -----------------------------------------------------------------------
// <copyright file="ITransformContract.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ITransformContract : IDisposable
    {
        /// <summary>
        /// Transform point
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        Result Transform(Result result);
    }
}
