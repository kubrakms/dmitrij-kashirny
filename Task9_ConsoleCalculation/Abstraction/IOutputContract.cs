﻿// -----------------------------------------------------------------------
// <copyright file="IOutputContract.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;

    /// <summary>
    /// Output contract
    /// </summary>
    public interface IOutputContract : IDisposable
    {
        /// <summary>
        /// Set data to output
        /// </summary>
        /// <param name="p"></param>
        void SetData(Result p);
    }
}
