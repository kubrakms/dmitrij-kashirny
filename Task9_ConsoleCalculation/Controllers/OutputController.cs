﻿// -----------------------------------------------------------------------
// <copyright file="OutputController.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;
    using System.IO;

    /// <summary>
    /// Output controller
    /// </summary>
    public class OutputController : IOutputContract
    {
        public IMode Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        
        public OutputController() { }

        public OutputController(IMode mode)
        {
            Mode = mode;
        }

        #region IOutputContract Members

        public void SetData(Result p)
        {
            if (Mode is Package )
            {
                string[] str = new string[p.Points.Count];

                for (int i = 0; i < p.Points.Count; i++)
                {
                    str [i] = string.Format("{0:F4};{1:F4};{2:F4}", p.Points[i].x, p.Points[i].y, p.Points[i].z);
                }                
                
                File.WriteAllLines(Command.outputFile, str);                
            }
            else if (Mode is Interactive)
            {
                Console.WriteLine(Command.CommandStrings.GetString("RESULT"), p.Points[0].x, p.Points[0].y, p.Points[0].z); 
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
       
        private IMode m_Mode;
    }
}
