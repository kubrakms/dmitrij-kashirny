﻿// -----------------------------------------------------------------------
// <copyright file="TransformController.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using geometry;

    /// <summary>
    /// Transform controller
    /// </summary>
    public class TransformController : ITransformContract
    {
        #region ILogicContract Members

        /// <summary>
        /// Transform the point
        /// </summary>
        /// <param name="p">Input point</param>
        /// <returns>Transformed point</returns>
        public Result Transform(Result inputResult)
        {
            Result transformResult = new Result();
            Gm.Transform myTransform = new Gm.Transform(Command.Matrix);

            foreach (Gm.Point str in inputResult.Points)
            {
                Gm.Point point = myTransform.TransformPoint(str);
                transformResult.Points.Add(point);
            }

            return transformResult;
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Implement Dispose() method
        /// </summary>
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
