﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;
    using geometry;
    using System.IO;

    /// <summary>
    /// Input controller
    /// </summary>
    public class InputController : IInputContract
    {
        public IMode Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }

        public InputController() { }

        public InputController(IMode mode)
        {
            Mode = mode;
        }

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region IInputContract Members

        public Result GetPoint()
        {
            double x = 0;
            double y = 0;
            double z = 0;
            string[] strPoints = null;
            Result inputResult = new Result();

            if (Mode is Package)
            {
                Package packageMode = Mode as Package;

                for (int i = 0; i < packageMode.Args.Length; i++)
                {
                    if (packageMode.Args[i].Contains(Command.CommandStrings.GetString("IN_FILE")))
                    {
                        Command.inputFile = packageMode.Args[i].Replace(Command.CommandStrings.GetString("IN_FILE"), string.Empty);
                    }
                    else if (packageMode.Args[i].Contains(Command.CommandStrings.GetString("OUTPUT_FILE")))
                    {
                        Command.outputFile = packageMode.Args[i].Replace(Command.CommandStrings.GetString("OUTPUT_FILE"), string.Empty);
                    }
                    else
                    {
                        inputResult.Error = 1;
                    }
                }

                if (File.Exists(Command.inputFile))
                {
                    strPoints = File.ReadAllLines(Command.inputFile);
                }
                else
                {
                    inputResult.Error = 1;
                }

                foreach (string str in strPoints)
                {
                    bool isDataValid = TryParse(str, out x, out y, out z);

                    if (!isDataValid)
                    {
                        inputResult.Error = 1;
                        break;
                    }
                    else
                        inputResult.Points.Add(new Gm.Point(x, y, z));
                }
            }
            else if (Mode is Interactive)
            {
                Console.WriteLine(Command.CommandStrings.GetString("Help"));

                Console.Write(Command.CommandStrings.GetString("COORDINATE_PROMT"));

                var str = Console.ReadLine();

                if (string.Compare(str, Command.CommandStrings.GetString("END"), true) == 0)
                    Environment.Exit(0);

                bool isValidData = TryParse(str, out x, out y, out z);

                inputResult.Points.Clear();
                inputResult.Points.Add(new Gm.Point(x, y, z));
            }

            return inputResult;
        }

        #endregion

        public bool TryParse(string s, out double x, out double y, out double z)
        {
            var data = s.Split(new char[] { ';' });
            bool isDataValid = Double.TryParse(data[0], out x) & Double.TryParse(data[1], out y) &
                                   Double.TryParse(data[2], out z);
            return isDataValid;
        }
        private IMode m_Mode;
    }
}
