﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace SoftServe_ConsoleCalculation
{
    class Program
    {
        const short FAULT_EXIT_CODE = -21312;
        const short SUCCSESS_EXIT_CODE = 0;

        static void Main(string[] args)
        {
            IMode mode;
            Result inputResult;
            Result transformResult;

            if (args.Length == 0)
                mode = new Interactive();
            else
                mode = new Package(args);

            do
            {
                using (var logic = LogicContainer.Instance.GetService<IInputContract>(new[] { mode }))
                {
                    inputResult = logic.GetPoint();
                }

                if (inputResult.Error == 0)
                {
                    using (var logic = LogicContainer.Instance.GetService<ITransformContract>(new[] { mode }))
                    {
                        transformResult = logic.Transform(inputResult);
                    }

                    if (transformResult.Error == 0)
                    {
                        using (var logic = LogicContainer.Instance.GetService<IOutputContract>(new[] { mode }))
                        {
                            logic.SetData(transformResult);
                        }
                    }
                    else
                    {
                        Environment.Exit(FAULT_EXIT_CODE);
                    }
                }
                else
                {
                    Environment.Exit(FAULT_EXIT_CODE);
                }
            }
            while (mode is Interactive);

            Environment.Exit(SUCCSESS_EXIT_CODE);
        }
    }
}
