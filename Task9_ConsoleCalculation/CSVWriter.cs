﻿// -----------------------------------------------------------------------
// <copyright file="" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CSVWriter
    {
        /// <summary>
        /// Write data to file
        /// </summary>
        /// <param name="filePath">File Name</param>
        /// <param name="data">Data to write</param>
        public void WriteToFile(string filePath, string[] data)
        {
            File.WriteAllLines(filePath, ConvertToCsv(data));

        }

        /// <summary>
        /// Convert data to csv format
        /// </summary>
        /// <param name="data">input data</param>
        /// <returns>data in csv format</returns>
        private string[] ConvertToCsv(string[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = data[i].Replace(' ', ';');
            }
            return data;
        }
    }
}
