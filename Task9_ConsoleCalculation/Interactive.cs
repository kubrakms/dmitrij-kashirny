﻿// -----------------------------------------------------------------------
// <copyright file="Interactive.cs" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe_ConsoleCalculation
{
    using System;
    using System.Collections.Generic;
    using geometry;

    /// <summary>
    /// Interactive mode class
    /// </summary>
    public class Interactive : IMode
    {
        #region IMode Members

        public List<Gm.Point> GetDataFromUser()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
