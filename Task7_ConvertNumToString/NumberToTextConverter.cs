﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7_ConvertNumToString
{
    public class NumberToTextConverter
    {

        Dictionary<int, string> numDictionary = new Dictionary<int, string>();
        Dictionary<int, string> numEngDictionary = new Dictionary<int, string>();
        Dictionary<int, string> tensDictionary = new Dictionary<int, string>();
        Dictionary<int, string> hundredsDictionary = new Dictionary<int, string>();

        #region Constryctor

        /// <summary>
        /// Constryctor
        /// </summary>
        public NumberToTextConverter()
        {
            Initialize();
        }

        #endregion

        #region Method NumToEnglishText

        public string NumToEnglishText(int num)
        {
            if (num < 20)
	        {
               return numEngDictionary[num]; 
	        }
            else
            {
                
            }
            return "";
        }

        #endregion

        #region Method Totext

        /// <summary>
        /// Convert number to text
        /// </summary>
        /// <param name="num"></param>
        /// <returns> string text </returns>
        public string ToText(int num)
        {
            if (num < 20)
            {
                return numDictionary[num];
            }
            else if (num < 100)
            {
                if (num % 10 == 0)
                {
                    return tensDictionary[num / 10];
                }
                else
                {
                    return tensDictionary[num / 10] + " " + numDictionary[num % 10];
                }
            }
            else if (num < 1000)
            {
                if (num % 100 == 0)
                {
                    return hundredsDictionary[num / 100];
                }
                else
                {
                    if (num % 100 == 0)
                    {
                        return hundredsDictionary[num / 100] + " " + numDictionary[num % 10];
                    }
                    else
                    {
                        return hundredsDictionary[num / 100] + " " + tensDictionary[(num % 100) / 10] + " " + numDictionary[num % 10];
                    }
                }
            }
            else
            {
                return "num > 1000";
            }
        }

        #endregion

        #region Initialize Dictionaries

        /// <summary>
        /// Init dictionaries
        /// </summary>
        private void Initialize()
        {
            numDictionary.Add(0, "ноль");
            numDictionary.Add(1, "один");
            numDictionary.Add(2, "два");
            numDictionary.Add(3, "три");
            numDictionary.Add(4, "четыре");
            numDictionary.Add(5, "пять");
            numDictionary.Add(6, "шесть");
            numDictionary.Add(7, "семь");
            numDictionary.Add(8, "восемь");
            numDictionary.Add(9, "девять");
            numDictionary.Add(10, "десять");
            numDictionary.Add(11, "одинадцась");
            numDictionary.Add(12, "двенадцать");
            numDictionary.Add(13, "тринадцать");
            numDictionary.Add(14, "четырнадцать");
            numDictionary.Add(15, "петнадцать");
            numDictionary.Add(16, "шестнадцать");
            numDictionary.Add(17, "семнадцать");
            numDictionary.Add(18, "восемнадцать");
            numDictionary.Add(19, "девятнадцать");

            tensDictionary.Add(2, "двадцать");
            tensDictionary.Add(3, "тридцать");
            tensDictionary.Add(4, "сорок");
            tensDictionary.Add(5, "пятдисят");
            tensDictionary.Add(6, "шестдисят");
            tensDictionary.Add(7, "семдисят");
            tensDictionary.Add(8, "восемдесят");
            tensDictionary.Add(9, "девяносто");

            hundredsDictionary.Add(1, "сто");
            hundredsDictionary.Add(2, "двести");
            hundredsDictionary.Add(3, "триста");
            hundredsDictionary.Add(4, "четыреста");
            hundredsDictionary.Add(5, "пятьсот");
            hundredsDictionary.Add(6, "шестьсот");
            hundredsDictionary.Add(7, "семьсот");
            hundredsDictionary.Add(8, "восемьсот");
            hundredsDictionary.Add(9, "девятьсот");

            numEngDictionary.Add(0, "zero");
            numEngDictionary.Add(1, "one");
            numEngDictionary.Add(2, "two");
            numEngDictionary.Add(3, "three");
            numEngDictionary.Add(4, "four");
            numEngDictionary.Add(5, "five");
            numEngDictionary.Add(6, "six");
            numEngDictionary.Add(7, "seven");
            numEngDictionary.Add(8, "eight");
            numEngDictionary.Add(9, "nine");
            numEngDictionary.Add(10, "ten");
            numEngDictionary.Add(11, "eleven");
            numEngDictionary.Add(12, "twelve");
            numEngDictionary.Add(13, "thirteen");
            numEngDictionary.Add(14, "fourteen");
            numEngDictionary.Add(15, "fifteen");
            numEngDictionary.Add(16, "sixteen");
            numEngDictionary.Add(17, "seventeen");
            numEngDictionary.Add(18, "eighteen");
            numEngDictionary.Add(19, "nineteen");
        }

        #endregion
    }
}
