﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Task7_ConvertNumToString
{
    class Program
    {

        static void Main(string[] args)
        {
            string str = string.Empty;
            int num = 0;
            NumberToTextConverter myConverter = new NumberToTextConverter();

            Console.OutputEncoding = Encoding.GetEncoding(1251);
            while (true)
            {
                Console.Write("Enter number: ");
                str = Console.ReadLine();

                if (!Int32.TryParse(str, out num))
                {
                    Console.WriteLine("!!! Bad number !!!");
                }
                else
                {
                    Console.WriteLine(myConverter.NumToEnglishText(num));
                }
            }

            Console.ReadLine();
        }
    }
}
