﻿// -----------------------------------------------------------------------
// <copyright file="Params.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.DP033.ChessBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    /// <summary>
    /// Class for saving parameters and data
    /// </summary>
    public class Params
    {
        #region Properties

        /// <summary>
        /// Properties for Size of board
        /// </summary>
        public int Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        /// <summary>
        /// Properties for data
        /// </summary>
        public string[] ChessBoard
        {
            get { return m_ChessBoard; }
            set { m_ChessBoard = value; }
        }

        #endregion

        #region Privat Fields

        private string[] m_ChessBoard;
        private int m_Size = 0;

        #endregion
    }
}
