﻿// -----------------------------------------------------------------------
// <copyright file="IChessBoardCreateContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.DP033.ChessBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IChessBoardCreateContract
    /// </summary>
    public interface IChessBoardCreateContract : IDisposable
    {
        /// <summary>
        /// Create the chess board image
        /// </summary>
        /// <param name="size">Size of board</param>
        /// <returns>Array of strings</returns>
        string[] CreateChessBord(int size);
    }
}
