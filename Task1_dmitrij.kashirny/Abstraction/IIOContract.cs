﻿// -----------------------------------------------------------------------
// <copyright file="" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftServe.DP033.ChessBoard
{
    /// <summary>
    /// IIOContract
    /// </summary>
    public interface IIOContract : IDisposable
    {
        /// <summary>
        /// Write a string to consol
        /// </summary>
        /// <param name="str">String to write</param>
        void WriteString(string str);

        /// <summary>
        /// Write data to Consol Window
        /// </summary>
        /// <param name="str">Array of strings</param>
        void WriteData(string[] str);

        /// <summary>
        /// Read data from Consol
        /// </summary>
        /// <returns>Readed data</returns>
        string ReadData();
    }
}
