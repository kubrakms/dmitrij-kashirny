﻿// -----------------------------------------------------------------------
// <copyright file="" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.DP033.ChessBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            Params param = new Params();

            using (var ioController = LogicContainer.Instance.GetService<IIOContract>())
            {
                int size = 0;

                ioController.WriteString(Resources.SIZE_PROMT);

                if (!Int32.TryParse(ioController.ReadData(), out size))
                {
                    ioController.WriteString(Resources.ERROR_DATA);
                }
                else
                {
                    param.Size = size;
                }

                using (var chessBoardController = LogicContainer.Instance.GetService<IChessBoardCreateContract>())
                {
                    param.ChessBoard = chessBoardController.CreateChessBord(param.Size);
                }

                ioController.WriteData(param.ChessBoard);
            }

            Console.ReadKey();
        }
    }
}
