﻿// -----------------------------------------------------------------------
// <copyright file="ChessBoardController.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.DP033.ChessBoard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    /// <summary>
    /// Chess Board Controller
    /// </summary>
    internal class ChessBoardController : IChessBoardCreateContract
    {
        #region IChessBoardCreateContract Members

        /// <summary>
        /// Create chess board
        /// </summary>
        /// <param name="size">Size of board</param>
        /// <returns>Array created chess board</returns>
        public string[] CreateChessBord(int size)
        {
            string[] chessBoard = new string[size];

            for (int i = 0; i < chessBoard.Length; i++)
            {
                chessBoard[i] = string.Empty;
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i % 2 != 0)
                    {
                        if (j % 2 != 0)
                        {
                            chessBoard[i] = chessBoard[i].Insert(chessBoard[i].Length, Resources.STAR);
                        }
                        else
                        {
                            chessBoard[i] = chessBoard[i].Insert(chessBoard[i].Length, Resources.SPACE);
                        }
                    }
                    else
                    {
                        if (j % 2 == 0)
                        {
                            chessBoard[i] = chessBoard[i].Insert(chessBoard[i].Length, Resources.STAR);
                        }
                        else
                        {
                            chessBoard[i] = chessBoard[i].Insert(chessBoard[i].Length, Resources.SPACE);
                        }
                    }
                }
            }

            return chessBoard;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
