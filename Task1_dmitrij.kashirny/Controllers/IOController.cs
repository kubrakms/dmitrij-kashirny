﻿// -----------------------------------------------------------------------
// <copyright file="" company="Soft Serve Academy">
//This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.DP033.ChessBoard
{
    internal class IOController : IIOContract
    {
        #region IIOContract Members

        /// <summary>
        /// Write data to Console Window
        /// </summary>
        /// <param name="str">Array of strings wiht to be writen</param>
        public void WriteData(string[] str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }
        }

        /// <summary>
        /// Read Line from Console
        /// </summary>
        /// <returns>Readed line</returns>
        public string ReadData()
        {
            string str = Console.ReadLine();
            return str;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region IIOContract Members

        public void WriteString(string str)
        {
            Console.Write(str);
        }

        #endregion
    }
}
