﻿// -----------------------------------------------------------------------
// <copyright file="IFormatContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Modules.HalpContractor
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents fpFormat utility behavior
    /// </summary>
    public interface IFormatContract : IDisposable
    {
        /// <summary>
        /// Transform file format with fpFormat utility
        /// </summary>
        /// <param name="inputFile">Input file path</param>
        /// <param name="outputFile">Output file path</param>
        /// <returns>Exit code: 0 - sucssess format; -21312 - fail format</returns>
        /// <exception cref="ArgumentException">When file not found</exception>
        int TransformFormat(string inputFile, string outputFile);
    }
}
