﻿// -----------------------------------------------------------------------
// <copyright file="ITransformContract.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.HalpWeb.Modules.HalpContractor
{
    using System;

    /// <summary>
    /// Represents fpTransform utility behavior
    /// </summary>
    public interface ITransformContract : IDisposable
    {
        /// <summary>
        /// Transform data throu fpTransform utiliyt
        /// </summary>
        /// <param name="inputFile">Input file name</param>
        /// <param name="outputFile">Output file name</param>
        /// <returns>Error code: 0 - sucssess transforming; -21312 - fail transforming</returns>
        int Transform(string inputFile, string outputFile);
    }
}
