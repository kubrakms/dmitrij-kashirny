﻿// -----------------------------------------------------------------------
// <copyright file="JSONReadWrite.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace SoftServe.HalpWeb.Modules.HalpData
{
    /// <summary>
    /// JSON Serialize/Desirealize class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JSONReadWriter<T> : IReadWrite<T>
    {
        private DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<T>));

        #region IReadWrite<T> Members

        /// <summary>
        /// Desirealize List of types T from .json file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <returns>List of types T</returns>
        public List<T> Read(string fileName)
        {
            List<T> result = new List<T>();

            using (FileStream rd = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                result = (List<T>)ser.ReadObject(rd);
            }

            return result;
        }

        /// <summary>
        /// Serialize List of types T in .json file
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <param name="data">List of types T</param>
        public void Write(string fileName, List<T> data)
        {
            using (FileStream wr = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                ser.WriteObject(wr, data);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
