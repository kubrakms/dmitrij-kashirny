﻿// -----------------------------------------------------------------------
// <copyright file="BINReadWriter.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace SoftServe.HalpWeb.Modules.HalpData
{
    /// <summary>
    /// Binary Serialezer/Desirealizer class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BINReadWriter<T> : IReadWrite<T>
    {

        #region IReadWrite<T> Members

        /// <summary>
        /// Desirealize List of types T from binary file.
        /// </summary>
        /// <param name="fileName">file with will be read</param>
        /// <returns>List of types T</returns>
        public List<T> Read(string fileName)
        {
            List<T> result = new List<T>();
            using (Stream reader = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                BinaryFormatter myFormater = new BinaryFormatter();
                myFormater.AssemblyFormat = FormatterAssemblyStyle.Simple;
                myFormater.Binder = new VersionConfigToNamespaceAssemblyObjectBinder();
                
                result = (List<T>)myFormater.Deserialize(reader);
            }

            return result;
        }

        /// <summary>
        /// Serialize List of types T in binary file
        /// </summary>
        /// <param name="fileName">input file</param>
        /// <param name="data">List of types T</param>
        public void Write(string fileName, List<T> data)
        {

            using (Stream writer = new FileStream(fileName, FileMode.Create))
            {
                BinaryFormatter myFormater = new BinaryFormatter();
                myFormater.AssemblyFormat = FormatterAssemblyStyle.Simple;
                
                myFormater.Serialize(writer, data);
            }
        }

        #endregion

        internal sealed class VersionConfigToNamespaceAssemblyObjectBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                Type typeToDeserialize = null;
                try
                {
                    string ToAssemblyName = assemblyName.Split(',')[0];
                    Assembly[] Assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    foreach (Assembly ass in Assemblies)
                    {
                        if (ass.FullName.Split(',')[0] == ToAssemblyName)
                        {
                            typeToDeserialize = ass.GetType(typeName);
                            break;
                        }
                    }
                }
                catch (System.Exception exception)
                {
                    throw exception;
                }
                return typeToDeserialize;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
