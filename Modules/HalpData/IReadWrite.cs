﻿// -----------------------------------------------------------------------
// <copyright file="IReadWrite.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace SoftServe.HalpWeb.Modules.HalpData
{
    /// <summary>
    /// Interface with realize Read and Write logic
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IReadWrite<T> : IDisposable
    {
        /// <summary>
        /// Read List<T> from file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>List<t></returns>
        List<T> Read(string fileName);
        
        /// <summary>
        /// Write List<T> to file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="data">List<T></param>
        void Write(string fileName, List<T> data);
    }
}
