﻿// -----------------------------------------------------------------------
// <copyright file="FormatFactory.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// ---------------------------------------------------------------------

namespace SoftServe.HalpWeb.Modules.HalpData
{
    

    /// <summary>
    /// Realization of Factory patern
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class FormatFactory<T>
    {
        const string XML = "xml";
        const string JSON = "json";
        const string CSV = "csv";
        const string BIN = "dat";

        /// <summary>
        /// Return instanse depending on format
        /// </summary>
        /// <param name="format">Format</param>
        /// <returns>Instanse depetding on format</returns>
        public static IReadWrite<T> GetInstance(string format)
        {
            if (string.Compare(format, XML) == 0)
            {
                return new XMLReadWriter<T>();
            }

            if (string.Compare(format, JSON) == 0)
            {
                return new JSONReadWriter<T>();
            }

            if (string.Compare(format, CSV) == 0)
            {
                return new CSVReadWrite<T>();
            }

            if (string.Compare(format, BIN) == 0)
            {
                return new BINReadWriter<T>();
            }

            return null;
        }
    }
}
