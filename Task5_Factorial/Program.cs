﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5_Factorial
{
    class Program
    {

        static int Factoryal(int num)
        {
            int result = 1;
            
            for (int i = 1; i <= num; i++)
                {
                    result = result * i;
                }
            
            return result;
        }
        
        static void Main(string[] args)
        {
            int num = 0;
            string str = string.Empty;
            
            Console.Write("Enter number: ");
            str = Console.ReadLine();

            if (!Int32.TryParse(str, out num))
            {
                Console.WriteLine("!!! Bab number !!!");
            }
            else
            {
                Console.WriteLine("{0}! = {1}", num, Factoryal(num));
            }

            Console.ReadLine();
        }
    }
}
