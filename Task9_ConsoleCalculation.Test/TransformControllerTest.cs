﻿// -----------------------------------------------------------------------
// <copyright file="TransformControllerTest.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace Task9_ConsoleCalculation.Test
{
    using geometry;
    using NUnit.Framework;
    using SoftServe_ConsoleCalculation;
    using System.Collections.Generic;

    /// <summary>
    /// Transform controller test class
    /// </summary>
    [TestFixture]
    public class TransformControllerTest
    {
        [Test(Description = "Examines Transform functionality"), TestCaseSource("SourcePoints")]
        public void TransformTest(Gm.Point inputPoint, Gm.Point etalonPoint)
        {
            Result result = new Result();
            result.Points.Add(inputPoint);

            using (var transformer = LogicContainer.Instance.GetService<ITransformContract>())
            {
                result = transformer.Transform(result);
            }

            Assert.AreEqual(result.Points[0], etalonPoint);
        }

        public static IEnumerable<TestCaseData> SourcePoints
        {
            get
            {
                yield return new TestCaseData(
                    new Gm.Point() { x = 5, y = 6, z = 7 },
                    new Gm.Point() { x = 5, y = 7, z = 8 }
                    );

                yield return new TestCaseData(
                    new Gm.Point() { x = 2, y = 546, z = 37 },
                    new Gm.Point() { x = 2, y = 547, z = 38 }
                    );

                yield return new TestCaseData(
                    new Gm.Point() { x = 5, y = 63, z = 75 },
                    new Gm.Point() { x = 5, y = 64, z = 76 }
                    );
            }
        }
    }
}
