﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public abstract class InsectBase : AnimalBase
    {
        #region Constants

        private const string TYPE = "insectum";
        private const int m_NumPairOfLegs = 3;
        private const int m_NumPairOfWings = 2;

        #endregion

        private bool m_IsPoison;

        #region Properties

        public string Type
        {
            get { return TYPE; }
        }

        public int NumPairOfLegs
        {
            get { return m_NumPairOfLegs; }
        }

        public int NumPairOfWings
        {
            get { return m_NumPairOfWings; }
        }

        public bool IsPoison
        {
            get { return m_IsPoison; }
            set { m_IsPoison = value; }
        }

        #endregion
    }
}
