﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo.Abstract
{
    interface ISerializator
    {
        void Serialize(string fileName, object data);
    }
}
