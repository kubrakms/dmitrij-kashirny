﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Animals
{
    public abstract class MammalBase : AnimalBase
    {
        #region Constants

        private const string TYPE = "endothermic vertebrates";
        private Covering m_Covering; 

        #endregion

        #region Properties
        
        public string Type
        {
            get { return TYPE; }
        }

        public Covering Covering
        {
            get { return m_Covering; }
            set { m_Covering = value; }
        }
        
        #endregion
    }
}
