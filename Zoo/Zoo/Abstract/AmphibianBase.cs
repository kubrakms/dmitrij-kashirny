﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public abstract class AmphibianBase : AnimalBase
    {
        #region Constatns

        private const string TYPE = "vertebrates";
        private const string THERMOREGULATION = "cool-blooded";
        private const string UNDER_WATER_RESPIRATION = "gills";
        private const string ON_LAND_RESPIRATION = "lungs";

        #endregion

        private bool m_IsPoison;

        #region Properties

        /// <summary>
        /// Type of amphibian
        /// </summary>
        public string Type
        {
            get { return TYPE; }
        }

        /// <summary>
        /// Type of under water respiration of a amphibian
        /// </summary>
        public string UnderWaterRespiration
        {
            get { return UNDER_WATER_RESPIRATION; }
        }

        /// <summary>
        /// Type of on land respiration of a amphibian
        /// </summary>
        public string OnLandRespiration
        {
            get { return ON_LAND_RESPIRATION; }
        }

        /// <summary>
        /// Type of thermoregulation of a amphibian
        /// </summary>
        public string Thermoregulation
        {
            get { return THERMOREGULATION; }
        }

        public bool IsPoison
        {
            get { return m_IsPoison; }
            set { m_IsPoison = value; }
        }


        #endregion
    }
}
