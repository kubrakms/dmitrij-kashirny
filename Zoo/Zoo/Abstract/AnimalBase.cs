﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zoo;
using System.Xml.Serialization;

namespace Animals
{
    public abstract class AnimalBase
    {
        /// <summary>
        /// Private fields
        /// </summary>
        #region Fields

        private int m_ZooId = 1;
        private string m_Name;
        private float m_Weight;
        private bool m_IsPredator;
        private Maturity m_Maturity;
        private Gender m_Gender;

        #endregion

        /// <summary>
        /// Base characteristics of animals
        /// </summary>
        #region Properties

        public int ZooId
        {
            get { return m_ZooId; }
            set { m_ZooId = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public float Weight
        {
            get { return m_Weight; }
            set { m_Weight = value; }
        }

        public bool IsPredator
        {
            get { return m_IsPredator; }
            set { m_IsPredator = value; }
        }

        public Maturity Maturity
        {
            get { return m_Maturity; }
            set { m_Maturity = value; }
        }

        public Gender Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }


        #endregion

        /// <summary>
        /// Methods represents what animals to do
        /// </summary>
        #region Methods
        
        public void Feed()
        {
            throw new NotImplementedException();
        }

        public virtual void Move()
        {
            throw new NotImplementedException();
        }

        public virtual void Reproduce()
        {
            throw new NotImplementedException();
        }

        public void Sleep()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
