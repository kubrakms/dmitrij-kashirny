﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    interface IKeeper
    {
        /// <summary>
        /// Method to move animal from one cage to other
        /// </summary>
        void MoveAnimal();

        /// <summary>
        /// Method for feedeng animal
        /// </summary>
        void FeedAnimal();

        /// <summary>
        /// Method for cure animal
        /// </summary>
        void CureAnimal();
    }
}
