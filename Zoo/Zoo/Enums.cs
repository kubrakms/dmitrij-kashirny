﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public enum Maturity : byte
    {
        Baby = 1,
        Teen = 2,
        Grown = 3,
        Old = 4
    };

    public enum Gender : byte
    { 
        Male = 1, 
        Female = 2 
    };

    public enum Covering : byte
    { 
        Fur = 1,
        Hair = 2
    };
}
