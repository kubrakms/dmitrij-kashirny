﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class Saturnia : InsectBase
    {
        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Saturnia()
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="weight"></param>
        /// <param name="speed"></param>
        /// <param name="zooId"></param>
        /// <param name="maturity"></param>
        /// <param name="gender"></param>
        /// <param name="isPoison"></param>
        /// <param name="isPredator"></param>
        public Saturnia(string name, float weight, float speed, Maturity maturity, Gender gender, bool isPoison, bool isPredator)
        {
            this.Name = name;
            this.Weight = weight;
            this.Maturity = maturity;
            this.Gender = gender;
            this.IsPoison = isPoison;
            this.IsPredator = isPredator;
        }

        #endregion
    }
}
