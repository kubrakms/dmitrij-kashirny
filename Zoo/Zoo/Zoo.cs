﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animals;

namespace Zoo
{
    public class Zoo
    {
        private static int m_IdZoo = 1;
        private List<Cell> m_Cells = new List<Cell>();
        private Keeper m_Keeper;

        public int IdZoo
        {
            get { return m_IdZoo; }
        }

        public List<Cell> Cells
        {
            get { return m_Cells; }
            set { m_Cells = value; }
        }

        public Keeper Keeper
        {
            get { return m_Keeper; }
            set { m_Keeper = value; }
        }
    }
}
