﻿using Animals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Cell
    {
        private List<AnimalBase> m_Animals = new List<AnimalBase>();

        public List<AnimalBase> Animals
        {
            get { return m_Animals; }
            set { m_Animals = value; }
        }
    }
}
