﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class Lion : MammalBase
    {
        #region Fields

        private float m_Speed;

        #endregion

        #region Properties

        /// <summary>
        /// Speed of lion in km/hour
        /// </summary>
        public float Speed
        {
            get { return m_Speed; }
            set { m_Speed = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Lion()
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name animal</param>
        /// <param name="weight">Weight animal</param>
        /// <param name="speed">Speed animal</param>
        /// <param name="zooId">Zoo Id</param>
        /// <param name="maturity">Maturity animal</param>
        /// <param name="gender">Gender animal</param>
        /// <param name="isPredator">is animal predator</param>
        public Lion(string name, float weight, float speed, Maturity maturity, Gender gender, Covering covering, bool isPredator)
        {
            this.Name = name;
            this.Weight = weight;
            this.Speed = speed;
            this.Maturity = maturity;
            this.Gender = gender;
            this.IsPredator = isPredator;
            this.Covering = covering;
        }

        #endregion
    }
}
