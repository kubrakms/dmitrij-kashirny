﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class Koala : MammalBase
    {
        # region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Koala()
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name animal</param>
        /// <param name="weight">Weight animal</param>
        /// <param name="speed">Speed animal</param>
        /// <param name="zooId">Zoo Id</param>
        /// <param name="maturity">Maturity animal</param>
        /// <param name="gender">Gender animal</param>
        /// <param name="isPredator">is animal predator</param>
        public Koala(string name, float weight, Maturity maturity, Gender gender, Covering covering, bool isPredator)
        {
            this.Name = name;
            this.Weight = weight;
            this.Maturity = maturity;
            this.Gender = gender;
            this.IsPredator = isPredator;
            this.Covering = covering;
        }

        #endregion
    }
}
