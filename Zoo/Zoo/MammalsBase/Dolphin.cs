﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class Dolphin : MammalBase
    {
        #region Fields

        private float m_Speed;

        #endregion

        #region Properties

        /// <summary>
        /// Speed of dolphin in km/hour
        /// </summary>
        public float Speed
        {
            get { return m_Speed; }
            set { m_Speed = value; }
        }

        #endregion

        #region Methods

        public override void Move()
        {

        }

        public override void Reproduce()
        {

        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Dolphin()
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="weight"></param>
        /// <param name="speed"></param>
        /// <param name="zooId"></param>
        /// <param name="maturity"></param>
        /// <param name="gender"></param>
        /// <param name="isPredator"></param>
        public Dolphin(string name, float weight, float speed, Maturity maturity, Gender gender, Covering covering, bool isPredator)
        {
            this.Name = name;
            this.Weight = weight;
            this.Speed = speed;
            this.Maturity = maturity;
            this.Gender = gender;
            this.IsPredator = isPredator;
            this.Covering = covering;
        }

        #endregion
    }
}
