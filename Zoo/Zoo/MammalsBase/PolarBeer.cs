﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Zoo;

namespace Animals
{
    public class PolarBeer : MammalBase
    {
        #region Methods

        public override void Move()
        {

        }

        public override void Reproduce()
        {

        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public PolarBeer()
        { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="weight"></param>
        /// <param name="zooId"></param>
        /// <param name="maturity"></param>
        /// <param name="gender"></param>
        /// <param name="isPredator"></param>
        public PolarBeer(string name, float weight, Maturity maturity, Gender gender, Covering covering, bool isPredator)
        {
            this.Name = name;
            this.Weight = weight;
            this.Maturity = maturity;
            this.Gender = gender;
            this.IsPredator = isPredator;
            this.Covering = covering;
        }

        #endregion
    }
}
