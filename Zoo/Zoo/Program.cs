﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animals;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Zoo
{
    class Program
    {
        static void Main(string[] args)
        {
            Zoo myZoo = new Zoo();
            myZoo.Keeper = new Keeper { Name = "Jon" };
            Cell cell1 = GetAnimal4Cell1();
            myZoo.Cells.Add(cell1);
            Cell cell2 = GetAnimal4Cell2();
            myZoo.Cells.Add(cell2);
            Cell cell3 = GetAnimal4Cell3();
            myZoo.Cells.Add(cell3);
            Cell cell4 = GetAnimal4Cell4();
            myZoo.Cells.Add(cell4);

            
            XmlSerializer x = new XmlSerializer(typeof(List<Cell>), new Type[] {typeof(Dolphin), typeof(PolarBeer), typeof(Lion), typeof(GoldenPoisonFrog),
                      typeof(ChineseGiantSalamander), typeof(Koala), typeof(Saturnia), typeof(GiantHornet), typeof(GiantAnts)});
            
            Stream stream = new FileStream("animals.xml", FileMode.Create);
            
            x.Serialize(stream, myZoo.Cells);
            Console.ReadLine();

        }

        public static Cell GetAnimal4Cell1()
        {
            Cell cellForReturn = new Cell();

            cellForReturn.Animals.Add(new PolarBeer("Boo", 450, Maturity.Teen, Gender.Male, Covering.Fur, true));
            cellForReturn.Animals.Add(new PolarBeer("Ann", 500, Maturity.Grown, Gender.Female, Covering.Fur, true));
            cellForReturn.Animals.Add(new Lion("Jack", 300, 78, Maturity.Old, Gender.Male, Covering.Fur, true));
            cellForReturn.Animals.Add(new Lion("Sonja", 450, 85, Maturity.Teen, Gender.Female, Covering.Fur, true));

            return cellForReturn;
        }

        public static Cell GetAnimal4Cell2()
        {
            Cell cellForReturn = new Cell();

            cellForReturn.Animals.Add(new Koala("Manny", 15, Maturity.Teen, Gender.Male, Covering.Fur, false));
            cellForReturn.Animals.Add(new Koala("Bonny", 5, Maturity.Baby, Gender.Female, Covering.Fur, false));
            cellForReturn.Animals.Add(new Saturnia("Djin", 0.05f, 5, Maturity.Baby, Gender.Female, false, false));
            cellForReturn.Animals.Add(new Saturnia("Willy", 0.01f, 5, Maturity.Old, Gender.Male, false, false));

            return cellForReturn;
        }

        public static Cell GetAnimal4Cell3()
        {
            Cell cellForReturn = new Cell();

            cellForReturn.Animals.Add(new Dolphin("Bart", 160, 60, Maturity.Teen, Gender.Male, Covering.Fur, false));
            cellForReturn.Animals.Add(new Dolphin("Darsy", 150, 50, Maturity.Grown, Gender.Female, Covering.Fur, false));
            cellForReturn.Animals.Add(new GoldenPoisonFrog("Sam", 0.6f, 2, Maturity.Grown, Gender.Male, true, false));
            cellForReturn.Animals.Add(new GoldenPoisonFrog("Froggy", 0.5f, 2, Maturity.Old, Gender.Female, true, false));
            cellForReturn.Animals.Add(new ChineseGiantSalamander("Max", 0.2f, 10, Maturity.Teen, Gender.Male, false, false));
            cellForReturn.Animals.Add(new ChineseGiantSalamander("Piter", 0.4f, 10, Maturity.Old, Gender.Male, false, false));

            return cellForReturn;
        }

        public static Cell GetAnimal4Cell4()
        {
            Cell cellForReturn = new Cell();

            cellForReturn.Animals.Add(new GiantHornet("Bill", 0.2f, 40, Maturity.Teen, Gender.Male, true, true));
            cellForReturn.Animals.Add(new GiantHornet("Sally", 0.4f, 35, Maturity.Grown, Gender.Female, true, true));
            cellForReturn.Animals.Add(new GiantAnts("Daiv", 0.05f, 5, Maturity.Old, Gender.Male, true, true));
            cellForReturn.Animals.Add(new GiantAnts("Den", 0.04f, 5, Maturity.Teen, Gender.Male, true, true));

            return cellForReturn;
        }
    }
}
