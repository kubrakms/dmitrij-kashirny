﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animals;

namespace Zoo
{
    public class Keeper : IKeeper
    {
        #region Fields

        private string m_Name;

        #endregion

        #region Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        #region Implement IKeeper

        /// <summary>
        /// Implement IKeeper MoveAnimal()
        /// </summary>
        public void MoveAnimal()
        {

        }

        /// <summary>
        /// Implement IKeeper CureAnimal()
        /// </summary>
        public void CureAnimal()
        {

        }

        /// <summary>
        /// Implement IKeeper FeedAnimal()
        /// </summary>
        public void FeedAnimal()
        {

        }

        #endregion
    }
}
